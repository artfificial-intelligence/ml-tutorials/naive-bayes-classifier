# 텍스트 데이터를 위한 Naive Bayes 분류기 <sup>[1](#footnote_1)</sup>

> <font size="3">텍스트 데이터 분석을 위해 Naive Bayes 분류기를 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [들어가며](./naive-bayes.md#intro)
1. [Naive Bayes 분류기란?](./naive-bayes.md#sec_02)
1. [텍스트 데이터에 대해 Naive Bayes 분류기는 어떻게 작동하는가?](./naive-bayes.md#sec_03)
1. [Naive Bayes 분류기의 장단점](./naive-bayes.md#sec_04)
1. [텍스트 데이터를 위한 Naive Bayes 분류기의 어플리케이션](./naive-bayes.md#sec_05)
1. [Python에서 Naive Bayes 분류기의 구현 방법](./naive-bayes.md#sec_06)
1. [마치며](./naive-bayes.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 7 — Naive Bayes Classifier for Text Data](https://levelup.gitconnected.com/ml-tutorial-7-naive-bayes-classifier-for-text-data-d123f0cae75d?sk=257d277a0e81971a9ac667b40cfde3fc)를 편역하였다.